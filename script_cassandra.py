from cassandra.cluster import Cluster
import cassandra.cluster
import csv
import re
from datetime import datetime
import load

from cassandra.cluster import Cluster
from load_data_projet import load_data

     
         
def limiteur2(data, limit):
    for i, d in enumerate(data):
        if i==limit:
            return None
        yield d


cluster = Cluster(['localhost'])
session = cluster.connect('tdefilip_projet')


#==============================================================================
# creation_table_question_1 = "CREATE TABLE question_1_v2( \
#                 station_id text,\
#                 latitude float,\
#                 longitude float,\
#                 year varint,\
#                 month varint,\
#                 day varint,\
#                 hour varint,\
#                 minute varint,\
#                 tmpf float,\
#                 dwpf float,\
#                 relh float,\
#                 drct float,\
#                 sknt float,\
#                 alti float,\
#                 vsby float,\
#                 gust float,\
#                 skyc1 text,\
#                 skyc2 text,\
#                 skyc3 text,\
#                 skyc4 text,\
#                 skyl1 float,\
#                 skyl2 float,\
#                 skyl3 float,\
#                 skyl4 float,\
#                 wxcodes text,\
#                 feel float,\
#                 metar text,\
#                 PRIMARY KEY ((station_id, year), month, hour, minute));"
# 
# session.execute(creation_table_question_1)
#==============================================================================
#==============================================================================
# creation_table_question_2 = "CREATE TABLE question_2( \
#                 station_id text,\
#                 latitude float,\
#                 longitude float,\
#                 year varint,\
#                 month varint,\
#                 day varint,\
#                 hour varint,\
#                 minute varint,\
#                 tmpf float,\
#                 dwpf float,\
#                 relh float,\
#                 drct float,\
#                 sknt float,\
#                 alti float,\
#                 vsby float,\
#                 gust float,\
#                 skyc1 text,\
#                 skyc2 text,\
#                 skyc3 text,\
#                 skyc4 text,\
#                 skyl1 float,\
#                 skyl2 float,\
#                 skyl3 float,\
#                 skyl4 float,\
#                 wxcodes text,\
#                 feel float,\
#                 metar text,\
#                 PRIMARY KEY ((year, month), day, hour, minute, station_id));"
# 
# session.execute(creation_table_question_2)
#==============================================================================
#==============================================================================
# def insert_1(csv, session):
#     gen = limiteur2(load.loadata(csv),  500)
#     # data = load_ata(csvfilename)
#     i = 0
#     for r in gen:
# 
#         data = (r["station"],r['lat'],r['lon'],r["timestamp"][0],r["timestamp"][1],
#                 r["timestamp"][2],r["timestamp"][3],r["timestamp"][4],r["tmpf"],
#                 r["dwpf"],r["relh"],r["drct"],r["sknt"],r["alti"],r["vsby"],r["gust"],
#                 r["skyc1"],r["skyc2"],r["skyc3"],r["skyc4"],r["skyl1"],r["skyl2"],
#                 r["skyl3"],r["skyl4"],r["wxcodes"],r["feel"],r["metar"])
#         req = """
#         INSERT INTO question_1_extract(station_id,latitude,longitude,year,month,day,
#         hour, minute,tmpf,dwpf,relh,drct,sknt,alti,vsby,gust,skyc1,skyc2,skyc3,
#         skyc4,skyl1,skyl2,skyl3,skyl4,wxcodes,feel,metar )
#         VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 
#         %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
#         """
#         session.execute(req, data)
# 
# insert_1('asos.csv', session)
#==============================================================================

#==============================================================================
# def insert_1(csv, session):
#     gen = load.loadata(csv)
#     # data = load_ata(csvfilename)
#     for r in gen:
#         data = (r["station"],r['lat'],r['lon'],r["timestamp"][0],r["timestamp"][1],
#                 r["timestamp"][2],r["timestamp"][3],r["timestamp"][4],r["tmpf"],
#                 r["dwpf"],r["relh"],r["drct"],r["sknt"],r["alti"],r["vsby"],r["gust"],
#                 r["skyc1"],r["skyc2"],r["skyc3"],r["skyc4"],r["skyl1"],r["skyl2"],
#                 r["skyl3"],r["skyl4"],r["wxcodes"],r["feel"],r["metar"])
#         req = """
#         INSERT INTO question_1(station_id,latitude,longitude,year,month,day,
#         hour, minute,tmpf,dwpf,relh,drct,sknt,alti,vsby,gust,skyc1,skyc2,skyc3,
#         skyc4,skyl1,skyl2,skyl3,skyl4,wxcodes,feel,metar )
#         VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 
#         %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
#         """
#         session.execute(req, data)
# 
# insert_1('asos.csv', session)
#==============================================================================

def insert_1(csv, session):
    gen = load.loadata(csv)
    # data = load_ata(csvfilename)
    for r in gen:
        data = (r["station"],r['lat'],r['lon'],r["timestamp"][0],r["timestamp"][1],
                r["timestamp"][2],r["timestamp"][3],r["timestamp"][4],r["tmpf"],
                r["dwpf"],r["relh"],r["drct"],r["sknt"],r["alti"],r["vsby"],r["gust"],
                r["skyc1"],r["skyc2"],r["skyc3"],r["skyc4"],r["skyl1"],r["skyl2"],
                r["skyl3"],r["skyl4"],r["wxcodes"],r["feel"],r["metar"])
        req = """
        INSERT INTO question_1_v2(station_id,latitude,longitude,year,month,day,
        hour, minute,tmpf,dwpf,relh,drct,sknt,alti,vsby,gust,skyc1,skyc2,skyc3,
        skyc4,skyl1,skyl2,skyl3,skyl4,wxcodes,feel,metar )
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 
        %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        """
        session.execute(req, data)

insert_1('asos.csv', session)





