from cassandra.cluster import Cluster
from functools import reduce
from matplotlib.ticker import MaxNLocator
import numpy as np
import matplotlib.pyplot as plt
from pyspark import SparkContext
import matplotlib.gridspec as gridspec
import math as mt
from mpl_toolkits.basemap import Basemap

sc = SparkContext.getOrCreate()
cluster = Cluster(['localhost'])
session = cluster.connect('tdefilip_projet')
    
#init map
plt.figure(figsize=(8,8))
map = Basemap(llcrnrlat=47.08, llcrnrlon=5.41, urcrnrlat=55.24, urcrnrlon=15.97,
              projection = 'tmerc', lat_0= 50.575, lon_0=10.148, resolution = 'i')
map.drawcountries(linewidth=0.5)
map.drawmapboundary(fill_color='aqua')
map.fillcontinents(color='coral', lake_color='aqua')
map.drawcoastlines()

def get_values(year, month, day, hour, var):
        requete = """SELECT latitude, longitude,{} 
                     FROM tdefilip_projet.question_2 where year = {} and month = {} and day = {} and hour = {}  
                     LIMIT 100000 ALLOW FILTERING;
                  """.format(var, year, month, day, hour)
        return session.execute(requete)

def calc_moy_key(truc):
        (key, (s0, s1)) = truc
        return (key,s1/s0)



def exec_question_2(variable ,label_variable, year, month, day, hour ):
    
    valeurs = get_values(year, month, day, hour, variable)
    valeurs_para = sc.parallelize(valeurs)
    valeurs_para = valeurs_para.filter(lambda a: a[2] is not None)
    map_valeurs = valeurs_para.map(lambda data: (str(data[0])+','+str(data[1]), np.array([1, data[2]])))
    reduced_valeurs = map_valeurs.reduceByKey(lambda a,b: a+b)
    moyenne_valeurs = reduced_valeurs.map(calc_moy_key)
    result = []
    for i in moyenne_valeurs.collect():
        result.append(i)
    result_points = np.array(result)
    
    for i, d in enumerate(result_points):
        label = result_points[i,1]
        label = float(label)
        label = round(label,2)
        lat,lon = result_points[i,0].split(',') 
        lat = float(lat)
        lon = float(lon)
        lat = round(lat,4)
        lon = round(lon,4)
        x,y = map(lon, lat)
        map.plot(x, y, 'bo', markersize=3)
        plt.text(x,y,label, fontsize=7)
    
    plt.title("Valeur des {} pour la date {}/{}/{} - {}h".format(label_variable,day,month,year,hour))
    plt.show()
    plt.savefig('map_{}_{}_{}_{}h_{}.png'.format(day, month, year, hour, label_variable))


exec_question_2("tmpf","temperature", 2012, 2, 22, 7)
#exec_question_2("alti","altitude", 2012, 2, 22, 7)
