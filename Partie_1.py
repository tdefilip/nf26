from cassandra.cluster import Cluster
from functools import reduce
from matplotlib.ticker import MaxNLocator
from pyspark import SparkContext
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import math as mt
import matplotlib.patches as mpatches
import matplotlib.lines as mlines

sc = SparkContext.getOrCreate()
cluster = Cluster(['localhost'])
session = cluster.connect('tdefilip_projet')
    
plt.figure(figsize=(8,8))
plt.axes()


def get_historique(station, var):
        requete = """SELECT year, month, day, hour, minute, {} 
                     FROM tdefilip_projet.question_1_v2 where station_id = '{}' 
                     LIMIT 100000 ALLOW FILTERING;
                  """.format(var, station)
        return session.execute(requete)
    
def get_historique_year(station, var, year):
        requete = """SELECT year, month, day, hour, minute, {} 
                     FROM tdefilip_projet.question_1_v2 where station_id = '{}' 
                     AND year = {}
                     LIMIT 100000 ALLOW FILTERING;
                  """.format(var, station, year)
        return session.execute(requete)    

def calc_moy_std_key(truc):
        (key, (s0, s1, s2)) = truc
        return (key,s1/s0, mt.sqrt( (s2/s0 - (s1/s0)**2) ) )
    
    
def exec_question_1(station, variable ,label_variable, year):
    
    #Récupération des donnees sur l'ensemble de l'intervalle
    historique = get_historique(station, variable)
    histo_para = sc.parallelize(historique)
    histo_para = histo_para.filter(lambda a: a[5] is not None)
    map_mois = histo_para.map(lambda data: (data[1], np.array([1, data[5], data[5]**2])))  
    reduced_mois = map_mois.reduceByKey(lambda a,b: a+b)
    moyenne_mois = reduced_mois.sortByKey(ascending = True).map(calc_moy_std_key)
    
    result = []
    for i in moyenne_mois.collect():
        result.append(i)
    result = np.array(result)
    
    plt.clf()
    plt.bar(result[0:3,0], result[0:3,1], yerr = result[0:3,2], color='blue', capsize = 10) 
    plt.bar(result[3:6,0], result[3:6,1], yerr = result[3:6,2], color='green', capsize = 10) 
    plt.bar(result[6:9,0], result[6:9,1], yerr = result[6:9,2], color='red', capsize = 10) 
    plt.bar(result[9:12,0], result[9:12,1], yerr = result[9:12,2], color='orange', capsize = 10) 
    
    blue_patch = mpatches.Patch(color='blue', label="Hiver")
    green_patch = mpatches.Patch(color='green', label="Printemps")
    red_patch = mpatches.Patch(color='red', label="Eté")
    orange_patch = mpatches.Patch(color='orange', label="Automne")
    
    #Traitement pour l'année choisie
    historique_year = get_historique_year(station, variable, year)
    histo_para_year = sc.parallelize(historique_year)
    histo_para_year = histo_para_year.filter(lambda a: a[5] is not None)
    
    map_year = histo_para_year.map(lambda data: (data[1], np.array([1, data[5], data[5]**2])))
    reduced_year = map_year.reduceByKey(lambda a,b: a+b)
    moyenne_year = reduced_year.sortByKey(ascending = True).map(calc_moy_std_key)
    
    result = []
    for i in moyenne_year.collect():
        result.append(i)
    
    result = np.array(result)
    
    plt.plot(result[:,0], result[:,1], color= "purple", marker = '.', markersize = 8) 
    
    marqueurs = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    xtick_labels = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet'
                    , 'aout', 'septembre', 'octobre', 'novembre', 'décembre']
    plt.xticks(marqueurs, xtick_labels, rotation=30)
    
    plt.ylabel("{}".format(label_variable))
    purple_line = mlines.Line2D([], [], color='purple',label='{}'.format(year))
    plt.legend(handles=[blue_patch,green_patch, red_patch, orange_patch, purple_line])
    
    plt.title("""Graphique des {} pour l'année {} et la station {} par rapport 
              aux moyennes de 2011 à 2015 """.format(label_variable,station, year))
    plt.show()
    plt.savefig("annee{}_{}.png".format(year, label_variable))  


exec_question_1("EDDF", "tmpf","temperature", 2012)
